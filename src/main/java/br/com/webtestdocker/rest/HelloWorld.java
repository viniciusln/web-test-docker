package br.com.webtestdocker.rest;

import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/api/hello")
public class HelloWorld
{

	@GET
	@Path("/world")
	@Produces(MediaType.TEXT_PLAIN)
	public String helloWorld()
	{
		return "Hello World: " + new Date();
	}

}
