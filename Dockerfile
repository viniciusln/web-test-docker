#################  BUILD #######################################
FROM maven:3-jdk-8-alpine as BUILD

#criando um maven repo para ser persistente entre os passos da build
RUN mkdir -p /usr/maven_repo

#primeira parte, download das dependências
RUN mkdir -p /usr/tmp
WORKDIR /usr/tmp
ADD pom.xml /usr/tmp/pom.xml
RUN mvn -Dmaven.repo.local=/usr/maven_repo dependency:go-offline
# estou explicitamente ignorando o código de retorno porque o mvn install vai acontecer mesmo depois
# por enquanto somente preciso fazer caches para evitar fazer download de tudo a cada rebuild
RUN mvn -Dmaven.repo.local=/usr/maven_repo install; exit 0

#ok, vamos compilar pra valer agora
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
ADD . /usr/src/app
RUN cp /usr/tmp/pom.xml /usr/src/app/pom.xml
RUN mvn -Dmaven.repo.local=/usr/maven_repo clean install

#################  RUNTIME  #######################################
FROM tomcat:8.5.30-jre8-alpine as RUNTIME

# limpa a pasta das aplicações
RUN rm -rf /usr/local/tomcat/webapps/*

# busca da primeira fase o binário compilado
COPY --from=BUILD /usr/src/app/target/*.war /usr/local/tomcat/webapps/ROOT.war

#ok, executa o tomcat
CMD ["catalina.sh", "run"]
